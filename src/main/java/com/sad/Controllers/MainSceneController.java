package com.sad.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.sad.Main;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

import static com.sad.Utils.FileUtilizer.*;

@Log4j2
public class MainSceneController implements Initializable {

    @FXML
    public JFXTextField buildFileTextField;

    @FXML
    public JFXTextField sheetFileTextField;

    @FXML
    public JFXComboBox<String> clientComboBox;

    @FXML
    public JFXTextField textField;

    @FXML
    public JFXButton goButton;

    private File build;
    private File sheet;
    private String jarPath;
    private FileChooser fileChooser;

    private ArrayList<String> clientsList;

    public void openPathToBuildFile() {
        build = fileChooser.showOpenDialog(null);
        buildFileTextField.setText(build.getName());
    }

    @SneakyThrows({java.io.FileNotFoundException.class, java.io.IOException.class})
    public void openPathToSheetFile() {
        sheet = fileChooser.showOpenDialog(null);
        Platform.runLater(() -> sheetFileTextField.setText(sheet.getName()));
        if (sheet != null) {
            clientsList = new ArrayList<>();
            @Cleanup FileInputStream fis = new FileInputStream(sheet);
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Row header = sheet.getRow(0);
            for (int i = 1; i < header.getLastCellNum(); i++) clientsList.add(header.getCell(i).toString());
            clientsList.add("Все");
            Platform.runLater(() -> clientComboBox.setItems(FXCollections.observableList(clientsList)));
            fis.close();
        } else {
            Platform.runLater(() -> sheetFileTextField.setText("File sheet isn`t chosen"));
        }
    }

    public void startBuildingForClients() {
        new Thread(() -> {
            try {
                Platform.runLater(() -> goButton.setDisable(true));

                @Cleanup FileInputStream fis = new FileInputStream(sheet);
                XSSFWorkbook workbook = new XSSFWorkbook(fis);
                XSSFSheet sheet1 = workbook.getSheetAt(0);
                XSSFSheet sheet2 = workbook.getSheetAt(1);

                Platform.runLater(() -> textField.setText("Unziping " + build.getName()));

                Files.createDirectories(Paths.get(jarPath + "/temp/"));
                unzip(build.getAbsolutePath(), jarPath + "/temp/");

                if (clientComboBox.getValue().equals("Все")) {
                    Row row = sheet1.getRow(0);
                    for (int i = 1; i < row.getLastCellNum(); i++)
                        buildForClient(i, sheet1, sheet2);
                } else {
                    String clientName = clientComboBox.getValue();
                    Row row = sheet1.getRow(0);
                    for (int i = 1; i < row.getLastCellNum(); i++) {
                        if (clientName.equals(row.getCell(i).toString()))
                            buildForClient(i, sheet1, sheet2);
                    }
                }
                Platform.runLater(() -> goButton.setDisable(false));
                Platform.runLater(() -> textField.setText("Completed!"));
            } catch (Exception e) {
                log.error("Error while creating build " +e.getMessage());
            }
        }).start();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fileChooser = new FileChooser();
        ArrayList<String> configurations = new ArrayList<>();
        configurations.add("Все");
        clientComboBox.setItems(FXCollections.observableList(configurations));
        clientComboBox.getSelectionModel().select(0);
        CodeSource codeSource = Main.class.getProtectionDomain().getCodeSource();
        File jarFile;
        try {
            jarFile = new File(codeSource.getLocation().toURI().getPath());
            jarPath = jarFile.getParentFile().getPath();
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
        }
    }


    private void buildForClient(int columnNum, XSSFSheet sheet1, XSSFSheet sheet2) throws IOException {
        String clientName = sheet1.getRow(0).getCell(columnNum).toString();
        log.info("Detected in sheet " + clientName);
        Platform.runLater(() -> textField.setText("Working on " + clientName));

        Files.createDirectories(Paths.get(jarPath + "/temp/" + clientName));
        FileUtils.copyFile(new File(jarPath + "/temp/Files/installers/updates.zip"),
                new File(jarPath + "/" + clientName + "/updates.zip"));

        Files.createDirectories(Paths.get(jarPath + "/" + clientName + "/plugins/"));
        Files.createDirectories(Paths.get(jarPath + "/" + clientName + "/plugins_server/"));

        FileUtils.copyDirectory(new File(jarPath + "/temp/Files/plugins/client/"),
                new File(jarPath + "/" + clientName + "/plugins/"));
        FileUtils.copyDirectory(new File(jarPath + "/temp/Files/plugins/server/"),
                new File(jarPath + "/" + clientName + "/plugins_server/"));

        StringBuilder serverplugins = new StringBuilder();
        StringBuilder clientplugins = new StringBuilder();

        int tempJ = 0;

        try {
            for (int j = 1; j < sheet1.getLastRowNum(); j++) {
                tempJ = j;
                if (sheet1.getRow(j).getCell(columnNum) != null)
                    if (String.valueOf(sheet1.getRow(j).getCell(columnNum).getNumericCellValue()).equals("1.0")) {
                        String pluginName = sheet1.getRow(j).getCell(0).getStringCellValue() + ".jar\n";
                        serverplugins.append(pluginName);
                    }
            }
        } catch (NullPointerException npe) {
            log.info("Bad value at row " + tempJ + " cell : " + columnNum);
        }

        try {
            for (int j = 1; j < sheet2.getLastRowNum(); j++) {
                tempJ = j;
                if (sheet2.getRow(j).getCell(columnNum) != null)
                    if (String.valueOf(sheet2.getRow(j).getCell(columnNum).getNumericCellValue()).equals("1.0")) {
                        String pluginName = sheet2.getRow(j).getCell(0).getStringCellValue() + ".jar\n";
                        clientplugins.append(pluginName);
                    }
            }
        } catch (NullPointerException npe) {
            log.info("Bad value at row " + tempJ + " cell : " + columnNum);
        }

        log.info(clientName);
        log.info("\nServer plugins:\n" + serverplugins.toString() + "\nClient plugins:\n" + clientplugins.toString());

        deleteFiles(jarPath + "/" + clientName + "/plugins/", clientplugins.toString());
        deleteFiles(jarPath + "/" + clientName + "/plugins_server/", serverplugins.toString());

        addToZip(jarPath + "/" + clientName + "/updates.zip",
                jarPath + "/" + clientName + "/plugins/");
    }

}