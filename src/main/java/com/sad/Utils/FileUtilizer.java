package com.sad.Utils;

import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

import java.io.File;
import java.util.Objects;

@Log4j2
public class FileUtilizer {

    /**
     * Unzip file with default pass "" - file should not be encrypted with pass
     *
     * @param source      absolute file path like /root/bla/bla/bla/Storm_build_234123412.zip
     * @param destination destination folder to unzip items
     */
    public static void unzip(@NonNull String source, @NonNull String destination) {
        log.info("Start unziping : " + source);

        try {
            ZipFile zipFile = new ZipFile(source);
            if (zipFile.isEncrypted()) zipFile.setPassword("");
            zipFile.extractAll(destination);

            log.info("Unzip completed : " + source);
        } catch (ZipException e) {
            log.error("Error in unzipping file. Cause: " + e.getMessage());
            log.error("Zip path " + source);
            log.error("Unziping destination path " + destination);
        }
    }

    /**
     * Unzip file with param zipPass - file could be encrypted or could not. If not just skip setting password
     *
     * @param source      absolute file path like /root/bla/bla/bla/Storm_build_234123412.zip
     * @param destination destination folder to unzip items
     * @param zipPass     password to remove encryption for file
     */
    public static void unzip(@NonNull String source, @NonNull String destination, @NonNull String zipPass) {
        log.info("Start unziping : " + source);
        try {
            ZipFile zipFile = new ZipFile(source);
            if (zipFile.isEncrypted()) zipFile.setPassword(zipPass);
            zipFile.extractAll(destination);

            log.info("Unzip completed : " + source);
        } catch (ZipException e) {
            log.error("Error in unzipping file. Cause: " + e.getMessage());
            log.error("Zip path " + source);
            log.error("Unziping destination path " + destination);
            log.error("Zip pass : \"" + zipPass + "\"");
        }
    }

    /**
     * Delete files in folder if filename contains in string
     *
     * @param fileDirectory path to folder where files should be deleted
     * @param stringPlugins string filename container
     */
    public static void deleteFiles(@NonNull String fileDirectory, @NonNull String stringPlugins) {
        log.info("Delete file via template...");

        for (File file : Objects.requireNonNull(new File(fileDirectory).listFiles())) {
            if (!stringPlugins.contains(file.getName())) {
                file.delete();
            }
        }

        log.info("Deleted files via template complete!");
    }

    /**
     * Add File or Folder into root of zip file, using default pass "" if file encrypted
     *
     * @param sourceZipFile absolute path of zip file
     * @param folderToAdd   absolute path of file\folder to add
     */
    public static void addToZip(@NonNull String sourceZipFile, @NonNull String folderToAdd) {
        log.info("Adding file " + folderToAdd + " to " + sourceZipFile);

        try {
            ZipFile zipFile = new ZipFile(sourceZipFile);
            if (zipFile.isEncrypted()) zipFile.setPassword("");
            zipFile.addFolder(folderToAdd, new ZipParameters());

            log.info("Zip completed : " + sourceZipFile + " <- " + folderToAdd);
        } catch (ZipException e) {
            log.error("Error while adding file to zip " + e.getMessage());
            log.error("File path " + folderToAdd);
            log.error("Zip path " + sourceZipFile);
        }
    }

}
